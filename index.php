<?php
// set strict error reporting and display errors
error_reporting( E_ALL | E_STRICT );
ini_set( 'display_errors', 1 );

// define application paths
define( 'BASE_PATH', __DIR__ . DIRECTORY_SEPARATOR );

// register the application autoloader
spl_autoload_register( function( $class ) {
    /** @noinspection PhpIncludeInspection */
    require BASE_PATH . 'classes' . DIRECTORY_SEPARATOR . $class . '.php';
    return true;
} );

// dispatch the request to the home 'controller'
$controller = new Home();
$controller->index();
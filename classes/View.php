<?php if ( !defined( 'BASE_PATH' ) ) die( 'Forbidden' );
/**
 * @property $title string
 */
class View
{
    #region Properties

    // properties which shouldn't be accessed by the view files
    protected $_append  = array();
    protected $_data    = array();
    protected $_name    = '';
    protected $_path    = '';
    protected $_prepend = array();

    #endregion

    #region Constructors

    /**
     * @param string $name
     * @param array  $prepend
     * @param array  $append
     */
    public function __construct( $name = '', $prepend = array(), $append = array() )
    {
        $this->setName( $name    );
        $this->prepend( $prepend );
        $this->append ( $append  );
    }

    #endregion

    #region Methods

    /**
     * @param View|string|array $view
     * @return self
     */
    public function append( $view )
    {
        $this->addViewToArray( $view, $this->_append );
        return $this;
    }

    /**
     * @param View|string|array $view
     * @return self
     */
    public function prepend( $view )
    {
        $this->addViewToArray( $view, $this->_prepend );
        return $this;
    }

    /**
     * @param array       $data    [Optional] Additional variables to be used with the view's $_data property
     * @param bool        $return  [Optional] If true, return the view as a string instead of outputting it directly
     * @return string|int          The rendered view as a string if $return is true or the number of characters written
     * @throws Exception           If any of the views do not exist
     */
    public function render( $data = array(), $return = false )
    {
        if ( !file_exists( $this->_path ) ) {
            throw new Exception( 'The view does not exist' );
        }

        ob_start();

        // backup the $_data array and combine it with $data
        $backup      = $this->_data;
        $this->_data = $data = array_merge( $this->_data, $data );

        /** @noinspection PhpIncludeInspection */
        require $this->_path;

        // restore the $_data array
        $this->_data = $backup;

        $closure = function( $view ) use ( $data ) {
            /** @var $view View */
          return $view->render( $data, true );
        };

        $current = ob_get_clean();
        $prepend = array_map( $closure, $this->_prepend );
        $append  = array_map( $closure, $this->_append  );
        $content = implode( '', array_merge( $prepend, array( $current ), $append ) );

        if ( $return ) {
            return $content;
        }

        echo $content;
        return strlen( $content );
    }

    /**
     * @param View|string|array $view
     * @param array $array
     * @return self
     */
    protected function addViewToArray( $view, &$array )
    {
        foreach( (array)$view as $view ) {
            if ( !$view instanceof self ) {
                $view = new self( $view );
            }
            $array[] = $view;
        }
        return $this;
    }

    #endregion

    #region Getters and Setters

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function __set( $name, $value )
    {
        $this->_data[$name] = $value;
    }

    /**
     * @param string $name
     * @return string
     */
    public function __get( $name )
    {
        if ( !array_key_exists( $name, $this->_data ) ) {
            $class = get_called_class();
            trigger_error( "Undefined property: {$class}::\${$name}" );
            return null;
        }

        $value = $this->_data[$name];

        if ( !$value instanceof Form && $value !== null ) {
            $value = htmlspecialchars( $value );
        }

        return $value;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName( $name )
    {
        $this->_name = $name;
        $this->_path = BASE_PATH . "view/{$this->_name}.phtml";
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    #endregion
}
<?php if ( !defined( 'BASE_PATH' ) ) die( 'Forbidden' );
abstract class Model
{
    /**
     * @param int|string $id
     * @return string The feed url
     */
    public static function extractFeedUrl( $id )
    {
        $id = preg_replace( '#^(http.+/id([0-9]+)|([0-9]+)$).*#', '\2\3', $id );

        if ( !is_numeric( $id ) ) {
            return false;
        }

        $url = "https://buy.itunes.apple.com"
                . "/WebObjects"
                . "/MZFinance.woa"
                . "/wa"
                . "/com.apple.jingle.app.finance.DirectAction"
                . "/subscribePodcast?id={$id}&wasWarnedAboutPodcasts=true";

        // curl a request to find the original feed address
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'iTunes/9.1.1' );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        $response = curl_exec( $ch );
        curl_close( $ch );

        if ( preg_match( '/feedURL.+?string>([^<]+)/', $response, $match ) ) {
            return $match[1];
        }

        return false;
    }
}
<?php if ( !defined( 'BASE_PATH' ) ) die( 'Forbidden' );
class Form
{
    #region Properties

    protected $action   = '';
    protected $class    = '';
    protected $elements = array();
    protected $method   = '';

    #endregion

    #region Constructors

    public function __construct( $elements = array(), $method = 'post', $class = '' )
    {
        $this->action   = $_SERVER['REQUEST_URI'];
        $this->class    = $class;
        $this->elements = is_array( $elements ) ? $elements : array();
        $this->method   = strcasecmp( $method, 'get' ) === 0 ? 'get' : 'post';
    }

    #endregion

    #region Methods

    /**
     * @param string $name
     * @param bool   $trim
     * @return bool
     */
    public function getSubmittedValue( $name, $trim = true )
    {
        $array = $_POST;

        if ( $this->method == 'get' ) {
            $array = $_GET;
        }

        if ( array_key_exists( $name, $array ) ) {
            if ( $trim ) {
                return trim( $array[$name] );
            }
            return $array[$name];
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        $buffer = array( sprintf( '<form action="%s" method="%s"',
                htmlspecialchars( $this->action ),
                htmlspecialchars( $this->method ) ) );

        if ( $this->class ) {
            $buffer[] = ' class="' . $this->class . '"';
        }

        $buffer[] = '>';

        foreach ( $this->elements as $name => $attributes ) {
            $append  = '';
            $label   = '';
            $group   = false;
            $prepend = '';
            $type    = '';
            $value   = null;
            $short   = false;

            foreach( array( 'append', 'group', 'label', 'prepend', 'type', 'value' ) as $key ) {
                if ( isset( $attributes[$key] ) ) {
                    $$key = $attributes[$key];
                    unset( $attributes[$key] );
                }
            }

            if ( $label ) {
                if ( $group ) {
                    $controlGroup = '<div class="control-group">';
                    $controls     = '<div class="controls">';
                    $labelClass   = ' class="control-label"';
                } else {
                    $controlGroup = '';
                    $controls     = '';
                    $labelClass   = '';
                }
                $buffer[] = "{$controlGroup}<label{$labelClass}>{$label}</label>{$controls}";
            }

            $buffer[] = $prepend;

            if ( is_array( $type ) ) {
                // element uses a subtype and full closing tag, e.g. <button type="submit"></button>
                $buffer[] = "<{$type[0]} type=\"{$type[1]}\"";
                $type     = $type[0];
            } else {
                // detect the type of element to determine whether it uses a short closing tag
                switch( $type ) {
                    case 'button':
                        break;
                    default:
                        $short = true;
                        $type = 'input type="' . $type . '"';
                }
                $buffer[] = '<' . $type;
            }

            $buffer[] = ' name="' . htmlspecialchars( $name ) . '"';

            foreach ( $attributes as $k => $v ) {
                $buffer[] = ' ' . $k . '="' . htmlspecialchars( $v ) . '"';
            }

            // check if the user has submitted a value if it does not have a default
            $submitted = $this->getSubmittedValue( $name, false );
            if ( $submitted && $value === null ) {
                $value = htmlspecialchars( $submitted );
            }

            if ( $value ) {
                $buffer[] = " value=\"{$value}\"";
            }

            if ( $short ) {
                $buffer[] = " />";
            } else {
                $buffer[] = ">{$value}</{$type}>";
            }

            $buffer[] = $append;

            if ( $label && $group ) {
                $buffer[] = '</div>';
                $buffer[] = '</div>';
            }
        }

        $buffer[] = '</form>';

        return implode( '', $buffer );
    }

    #endregion
}
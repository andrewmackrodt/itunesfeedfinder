<?php if ( !defined( 'BASE_PATH' ) ) die( 'Forbidden' );
class Home
{
    /**
     * @var View
     */
    protected $view;

    public function __construct()
    {
        $this->view = new View( 'index', array( 'header' ), array( 'footer' ) );
    }

    public function index()
    {
        $formInputs = array(
            'id' => array(
                'label'       => '<strong>iTunes Feed ID</strong>',
                'prepend'     => '<div class="input-prepend"><span class="add-on"><i class="icon-music"></i></span>',
                'placeholder' => 'iTunes Feed ID',
                'type'        => 'text',
                'style'       => 'width: 520px',
                'append'      => '</div>' ),
            'submit' => array(
                'type'  => array( 'button', 'submit' ),
                'class' => 'btn btn-primary pull-right',
                'value' => 'Find' )
        );

        $form = new Form( $formInputs, 'get', 'form-horizontal' );
        $id   = $form->getSubmittedValue( 'id' );
        $feed = null;

        if ( $id ) {
            $feed = Model::extractFeedUrl( $id );
        }

        $this->view->feed = $feed;
        $this->view->form = $form;
        $this->view->title = 'iTunes Podcast Feed Finder';
        $this->view->render();
    }
}